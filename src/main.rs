use std::env;
use std::process;
use std::str::FromStr;

use omega_i2c_exp::pwm;

fn print_usage() {
    println!("usage: omega-pwm <command> <arguments>");
}

fn init() {
    pwm::driver_init().expect("Driver init");
}

fn sleep() {
    pwm::disable_chip().expect("Disble chip");
}

fn set_freq(argument1: &str, argument2: &str) {
    pwm::check_init().expect("Check init");

    let frequency = pwm::FREQUENCY_DEFAULT;
    let delay = 0.0;

    let channel = u8::from_str_radix(argument1, 10).expect("Parse channel");

    let duty = f64::from_str(argument2).expect("Parse duty cycle");

    pwm::set_frequency(frequency).expect("Set frequency");

    pwm::setup_driver(channel, duty, delay).expect("Setup driver");
}

fn main() {
    let args: Vec<String> = env::args().skip(1).collect();

    if args.len() == 0 || args.len() > 3 {
        print_usage();
        process::exit(1);
    }

    pretty_env_logger::init();

    let command = &args[0];

    match command.as_str() {
        "init" => init(),
        "set_freq" => set_freq(&args[1], &args[2]),
        "sleep" => sleep(),
        _ => panic!("Unsupported command: {}", command),
    }
}
